# Python Scripts


### Prerequisites
You'll need to install and configure the following development tools.

* `Python 3.11.1`
* `pip`
* `Pyenv`*

(*) You need to make sure you are using `Python 3.11.1`. You can use any way you want to install `Python 3.11.1` in your system. `Pip` is also a requirement.
`Pyenv` is a nice tool to Manage Python Versions, so it is the recommended tool to use. You can learn more about `Pyenv` and how to install it in the next section.

## Pyenv
pyenv lets you easily switch between multiple versions of Python. It's simple, unobtrusive, and follows the UNIX tradition of single-purpose tools that do one thing well.

### What pyenv _does..._

* Lets you **change the global Python version** on a per-user basis.
* Provides support for **per-project Python versions**.
* Allows you to **override the Python version** with an environment
  variable.
* Searches for commands from **multiple versions of Python at a time**.
  This may be helpful to test across Python versions with [tox](https://pypi.python.org/pypi/tox).


### In contrast with pythonbrew and pythonz, pyenv _does not..._

* **Depend on Python itself.** pyenv was made from pure shell scripts.
  There is no bootstrap problem of Python.
* **Need to be loaded into your shell.** Instead, pyenv's shim
  approach works by adding a directory to your `PATH`.
* **Manage virtualenv.** Of course, you can create [virtualenv](https://pypi.python.org/pypi/virtualenv)
  yourself, or [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv)
  to automate the process.

**Installation**, let's check how to install Pyenv from the official [documentation](https://github.com/pyenv/pyenv#getting-pyenv)

## Python 3.10.4 (using Pyenv)

To install Python 3.9.12 using pyenv run this command:
```bash
~# pyenv install 3.11.1
```
You can check the installed versions with:
```bash
~# pyenv versions
```
You can enable by default a version by using the 'local' options.
```bash
~# pyenv local 3.11.1
```
This will be local to the current folder.
You can also set up the 'global' version as wel with:
```bash
~# pyenv global 3.11.1
```

## `pip`
If you've successfully installed Python 3.9 then `pip` should already be installed

```bash
~# pyenv which pip
```
should respond with
```
/Users/nsolinis/.pyenv/versions/3.10.4/bin/pip
```
## Shared library Retail Device Health

We have introduced a python package retail-device-health which is hosted in the pypi repository in nexus.
The repository is called py_code
when pip is interacting with nexus on https://nexusczr.shared-tooling.aws-us-east-1.usp.czrs.io:8443/, it needs an environment variable
to be set. Set the environment variable as shown below. The example is for mac.

```bash
export REQUESTS_CA_BUNDLE=$(pwd)/ca-certificates.crt
```

The ca-certificates.crt file is available in this git repository

### Get the code

```bash
git clone <this_repo>
cd <this_repo>
```

### Creating your Python Virtual Environment

You can easily create your virtual environment following the next steps:
```bash
pyenv local 3.11.1
export REQUESTS_CA_BUNDLE=$(pwd)/ca-certificates.crt
python -m venv env
cp pip.conf env/
source env/bin/activate
pip install -U -r requirements_dev.txt
pytest --cov=app --cov-branch --cov-report term-missing tests/
```
_Note: You only need to use `pyenv local 3.10.4` if you are using `pyenv` to manage your different python versions. You need to make sure that you are
using **Python 3.10.4**, to make sure that this is the case, run `python --version` before building the python environment_.

- The virtual environment is located in the root folder of the project with the name `env`
- The `pip.conf` file contains all the configuration to be able to install all the required libraries
- During development you need to use the file `requirements_dev.txt` to install all the testing libraries (i.e. `pytest`)
- For production we need to build the environment using the file `requirements_lock.txt` (this includes all the
  versions for all the libraries that are required, plus the versions of any dependency of these libraries). This is
  what is called `freezing the environment`
- You can clear your environment by running `pip freeze | xargs pip uninstall -y`
- We are using Playwright to run some test that require a Chromium instance running, this is way you need to run `playwright install chromium `

### Test
[PyTest](https://pytest.org/en/latest/) is used for testing.  Unit tests are stored in the `tests` folder

Run them with (in an active Python Virtual Env.):
```bash
pytest tests
```

You can also run the test coverage to get a report.
```bash
pytest --cov=app --cov-branch --cov-report term-missing tests/
```

You can also include the option `--hypothesis-show-statistics` to show the Hypothesis statistics:
```bash
pytest --cov=app --cov-branch --cov-report term-missing --hypothesis-show-statistics tests/
```

### Installing new libraries or upgrading new version

#### Installation of a new library
You can manually add the new library and its version in the `requirements.txt` file. For instance, let's say
that you want to install the library called `dpath`. You need to add this new line to the `requirements.txt` file:
```
dpath==2.0.6
```
To install it you need to run:
```bash
pip install -U -r requirements.txt
```

#### Upgrading a version of a library
You can add the new version in the `requirements.txt` file. To install it you need to run:
```bash
pip install -U -r requirements.txt
```

###  Keeping the lock file up-to-date

> You need to make sure you are keeping the lock file up to date. As soon as you upgrade or install a new library
> **you need to make sure you ALWAYS UPDATE the lock file**. In order to do so you need to run this command:
> ```bash
> pip install pip-tools
> pip-compile --output-file=requirements_lock.txt requirements.txt
> ```
> _**Note**_: You only need to install `pip-tools` once. With `pip-compile` from the `pip-tools` project, you can update
> the `requirements_lock.txt` with the latest changes done in the `requirements.txt` (upgrades or new installations)

#### The Lock File
The `requirements_lock.txt` file contains the exact version specifiers and can be used to replicate your environment.
You’ve ensured that when your users install the packages listed in `requirements_lock.txt` into their own environments,
they’ll be using the versions that you intend them to use.
> _Freezing your requirements is an important step to ensure that your Python project works the same way for your users
> in their environments as it did in yours_.
