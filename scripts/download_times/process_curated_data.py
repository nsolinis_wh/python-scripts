import csv
from datetime import time
from pathlib import Path
from typing import Tuple

import matplotlib.pyplot as plt
import pandas as pd

from scripts.download_times import generate_series_data, ROOT_DIR, get_statistics, to_seconds


def to_seconds_from_time(the_time: time) -> int:
    return (the_time.hour * 60 + the_time.minute) * 60 + the_time.second


def to_time_object(the_time: str) -> time:
    return time.fromisoformat(the_time)


def extract_state_data(states_data: dict, state: str, zsync_took: time, time_stamp: str):
    time_values: list = states_data.get(state)
    if time_values is None:
        new_times = [{
            'zsync_took': zsync_took,
            'time_stamp': time_stamp
        }]
        states_data[state] = new_times
    else:
        time_values.append({
            'zsync_took': zsync_took,
            'time_stamp': time_stamp
        })


def extract_venue_data(venues_data: dict, venue_id: str, zsync_took: time, time_stamp: str):
    if venues_data is not None and len(venue_id) > 0:
        time_values: list = venues_data.get(venue_id)
        if time_values is None:
            new_times = [{
                'zsync_took': zsync_took,
                'time_stamp': time_stamp
            }]
            venues_data[venue_id] = new_times
        else:
            time_values.append({
                'zsync_took': zsync_took,
                'time_stamp': time_stamp
            })


def process_curated_data() -> Tuple[dict, dict]:
    with open(f'{ROOT_DIR}/data/curated_data.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)  # skip the headers

        states_data = dict()
        venues_data = dict()
        for row in csv_reader:
            state = row[0]
            venue_id = row[1]
            slot_id = row[2]
            component = row[3]
            zsync_took = to_time_object(row[4])
            time_stamp = row[5]

            extract_state_data(states_data, state, zsync_took, time_stamp)
            extract_venue_data(venues_data, venue_id, zsync_took, time_stamp)

        return states_data, venues_data


def extract_zsync_time(venue_data: list):
    return [data['zsync_took'] for data in venue_data]


def plot_venues(venues_data: dict):
    sorted_venues = sorted(venues_data, reverse=True)
    venues = list()
    to_plot = list()
    for venue_id in sorted_venues:
        venues.append(venue_id)
        venue_data = extract_zsync_time(venues_data[venue_id])
        to_plot.append(generate_series_data(venue_data))

    df = pd.DataFrame(to_plot, index=venues)
    filepath = Path(f'{ROOT_DIR}/data/venues_summary.csv')
    df.to_csv(filepath)

    df.plot(kind="barh", stacked=True, figsize=(12, 10))
    plt.tight_layout()
    plt.show()


def plot_states(states_data: dict):
    sorted_states = sorted(states_data)
    states_data_max = dict({
        'state': list(),
        'time': list()
    })
    states_data_mean = dict({
        'state': list(),
        'time': list()
    })
    states_data_min = dict({
        'state': list(),
        'time': list()
    })
    states = list()
    to_plot = list()
    for state in sorted_states:
        states.append(state)
        state_data = extract_zsync_time(states_data[state])
        to_plot.append(generate_series_data(state_data))
        max_time, mean_time, min_time = get_statistics(state_data)
        states_data_max['state'].append(state)
        states_data_max['time'].append(to_seconds(max_time))

        states_data_mean['state'].append(state)
        states_data_mean['time'].append(to_seconds(mean_time))

        states_data_min['state'].append(state)
        states_data_min['time'].append(to_seconds(min_time))

    df = pd.DataFrame(to_plot, index=states)
    pd.set_option('expand_frame_repr', False)
    print(df)
    filepath = Path(f'{ROOT_DIR}/data/state_summary.csv')
    df.to_csv(filepath)
    df.plot(kind="bar", stacked=False, width=0.9)

    df_max = pd.DataFrame(states_data_max, index=states)
    df_max.plot(kind="bar", width=0.9)
    df_mean = pd.DataFrame(states_data_mean, index=states)
    df_mean.plot(kind="bar", width=0.9)
    df_min = pd.DataFrame(states_data_min, index=states)
    df_min.plot(kind="bar", width=0.9)

    # define subplot layout
    # fig, axes = plt.subplots(nrows=3, ncols=1)
    # df_max.plot(kind="bar", ax=axes[0])
    # df_mean.plot(kind="bar", ax=axes[1])
    # df_min.plot(kind="bar", ax=axes[2])

    plt.tight_layout()
    plt.show()


def plot_by_state(states_data: dict, state: str):
    times_to_plot = list()
    state_data = extract_zsync_time(states_data[state])
    for time_delta in state_data:
        times_to_plot.append(to_seconds_from_time(time_delta))

    ts = pd.Series(times_to_plot)
    df = pd.DataFrame(pd.Series(times_to_plot), index=ts.index, columns=['download times'])

    plt.figure()
    df.plot(
        title=f'{state} USP download times',
        xlabel='Instances (examples)',
        ylabel='Time is seconds'
    )

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    the_states_data, the_venues_data = process_curated_data()
    plot_states(the_states_data)
    plot_venues(the_venues_data)
    plot_by_state(the_states_data, 'IN')
