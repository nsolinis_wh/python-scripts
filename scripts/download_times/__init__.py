import os
from typing import Tuple

import pandas as pd
from pandas import Timedelta, Series

ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '.'))


def to_seconds(time_delta: Timedelta) -> int:
    return int(time_delta.total_seconds())


def get_statistics(time_values: list) -> Tuple[Timedelta, Timedelta, Timedelta]:
    df = pd.DataFrame({'time': time_values})
    time_delta = pd.to_timedelta(df['time'].astype(str))
    max_time: Timedelta = time_delta.max()
    mean_time: Timedelta = time_delta.mean()
    min_time: Timedelta = time_delta.min()
    return max_time, mean_time, min_time


def generate_series_data(time_values: list) -> Series:
    max_time, mean_time, min_time = get_statistics(time_values)
    return pd.Series(
        [to_seconds(max_time), to_seconds(mean_time), to_seconds(min_time)],
        index=["max", "mean", "min"])
