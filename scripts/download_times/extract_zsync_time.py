import csv
import json
from datetime import time
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
from pandas import Timedelta

from scripts.download_times import to_seconds, generate_series_data, ROOT_DIR


def extract_venue_id(slot_id_string: str) -> str:
    return slot_id_string[:slot_id_string.find("/")]


def extract_component(log_message: str) -> str:
    index_last_slash = log_message.rfind("/") + 1
    index_last_hyphen = log_message.rfind("-")
    return log_message[index_last_slash:index_last_hyphen]


def extract_time(log_message: str) -> time:
    index_zsync_took = log_message.find('Zsync took ') + len('Zsync took ') + 1
    index_to_download = log_message.find('to download') - 2
    the_time = '0' + log_message[index_zsync_took:index_to_download]
    return time.fromisoformat(the_time)


def time_summary(time_values: list) -> dict:
    df = pd.DataFrame({'time': time_values})
    time_delta = pd.to_timedelta(df['time'].astype(str))
    mean_time: Timedelta = time_delta.mean()
    min_time: Timedelta = time_delta.min()
    max_time: Timedelta = time_delta.max()
    return {
        'max': f"{to_seconds(max_time)}s",
        'mean': f"{to_seconds(mean_time)}s",
        'min': f"{to_seconds(min_time)}s",
        'examples': len(time_values)
    }


def save_curated_data(curated_data: list) -> None:
    with open(f'{ROOT_DIR}/data/curated_data.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        for data in curated_data:
            spamwriter.writerow(data)


def extract_time_stamp(log_message):
    # log_message looks like "2023-02-17 10:57:50,347 - INFO [app.handler.util.z_sync_util._internal .."
    index_hyphen = log_message.find(' - ')
    return log_message[:index_hyphen]


def process_csv_file():
    with open(f'{ROOT_DIR}/data/30_days_zsync_took.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)  # skip the headers

        line_count = 0
        components_time = dict()
        curated_data = list()
        curated_data.append(['state', 'venue_id', 'slot_id', 'component', 'zsync_took', 'time_stamp', 'log_message'])
        for row in csv_reader:
            state = row[2]
            slot_id = row[3]
            venue_id = extract_venue_id(row[3])
            log_message = row[5]
            component = extract_component(log_message)
            zsync_took = extract_time(log_message)
            time_stamp = extract_time_stamp(log_message)

            if component not in ['product_rosi_device', 'device-messaging', 'docker-monitor']:

                time_values: list = components_time.get(component)
                if time_values is None:
                    new_times = [zsync_took]
                    components_time[component] = new_times
                else:
                    time_values.append(zsync_took)
                line_count += 1

            curated_data.append([state, venue_id, slot_id, component, zsync_took, time_stamp, log_message])
        print(f'Processed {line_count} lines.')

        save_curated_data(curated_data)

        components_summary = dict()
        data = dict()
        to_plot = list()
        components = list()
        for c in components_time:
            components.append(c)
            components_summary[c] = time_summary(components_time[c])
            data[c] = generate_series_data(components_time[c])
            to_plot.append(data[c])

        pretty_json = json.dumps(components_summary, indent=4)
        print(pretty_json)

        df = pd.DataFrame(to_plot, index=components)
        df.info()

        df.plot(kind="barh", stacked=True, grid=False,
                title=f'Download times (data from the last 30 days)',
                xlabel='Download time (seconds)', ylabel='Components')

        plt.tight_layout()
        plt.show()

        pd.set_option('expand_frame_repr', False)
        print(df)
        filepath = Path('summary_components.csv')
        df.to_csv(filepath)


if __name__ == '__main__':
    process_csv_file()
