import io
import os
import time

import fitz
from PIL import Image

print(fitz.__doc__)


def recoverpix(doc, item):
    xref = item[0]  # xref of PDF image
    smask = item[1]  # xref of its /SMask

    # special case: /SMask or /Mask exists
    if smask > 0:
        pix0 = fitz.Pixmap(doc.extract_image(xref)["image"])
        if pix0.alpha:  # catch irregular situation
            pix0 = fitz.Pixmap(pix0, 0)  # remove alpha channel
        mask = fitz.Pixmap(doc.extract_image(smask)["image"])

        try:
            pix = fitz.Pixmap(pix0, mask)
        except:  # fallback to original base image in case of problems
            pix = fitz.Pixmap(doc.extract_image(xref)["image"])

        if pix0.n > 3:
            ext = "pam"
        else:
            ext = "png"

        return {  # create dictionary expected by caller
            "ext": ext,
            "colorspace": pix.colorspace.n,
            "image": pix.tobytes(ext),
        }

    # special case: /ColorSpace definition exists
    # to be sure, we convert these cases to RGB PNG images
    if "/ColorSpace" in doc.xref_object(xref, compressed=True):
        pix = fitz.Pixmap(doc, xref)
        pix = fitz.Pixmap(fitz.csRGB, pix)
        return {  # create dictionary expected by caller
            "ext": "png",
            "colorspace": 3,
            "image": pix.tobytes("png"),
        }
    return doc.extract_image(xref)


dimlimit = 0  # 100  # each image side must be greater than this
relsize = 0  # 0.05  # image : image size ratio must be larger than this (5%)
abssize = 0  # 2048  # absolute image size limit 2 KB: ignore if smaller
imgdir = "output"  # found images are stored in this subfolder


def extract_images_from_doc(doc):
    t0 = time.time()

    page_count = doc.page_count  # number of pages
    xref_list = []
    img_list = []
    for pno in range(page_count):
        print(f"Extracting Images in page {pno + 1}")

        il = doc.get_page_images(pno)
        img_list.extend([x[0] for x in il])
        image_index = 0
        for img in il:
            xref = img[0]
            if xref in xref_list:
                continue
            width = img[2]
            height = img[3]
            if min(width, height) <= dimlimit:
                continue
            image = recoverpix(doc, img)
            n = image["colorspace"]
            imgdata = image["image"]

            if len(imgdata) <= abssize:
                continue
            if len(imgdata) / (width * height * n) <= relsize:
                continue
            image_index = image_index + 1
            image_ext = image["ext"]
            image_file_name = f"image_p{pno + 1}_img{image_index}.{image_ext}"
            # image_file_name = "img%05i.%s" % (xref, image["ext"])
            img_file = os.path.join(imgdir, image_file_name)
            file = open(img_file, "wb")
            file.write(imgdata)
            file.close()
            xref_list.append(xref)

    t1 = time.time()
    img_list = list(set(img_list))
    print(len(set(img_list)), "images in total")
    print(len(xref_list), "images extracted")
    print("total time %g sec" % (t1 - t0))


def func(s):
    s = s.replace("\n", " ")
    return ' '.join(s.split())


def extract_images(the_page, the_page_index):
    image_list = the_page.get_images()
    # printing number of images found in this page
    if image_list:
        print(f"[+] Found a total of {len(image_list)} images in page {the_page_index}")
    else:
        print("[!] No images found on page", the_page_index)
    for image_index, img in enumerate(the_page.get_images(), start=1):
        # get the XREF of the image
        xref = img[0]
        # extract the image bytes
        base_image = doc.extract_image(xref)
        image_bytes = base_image["image"]
        # get the image extension
        image_ext = base_image["ext"]
        # load it to PIL
        image = Image.open(io.BytesIO(image_bytes))
        # save it to local disk
        image.save(open(f"img/image{the_page_index + 1}_{image_index}.{image_ext}", "wb"))


def extract_text(the_page, pno):
    print(f"Extracting Text in page {pno + 1}")
    text = the_page.get_text()
    if text is not None and len(text) > 0:
        with open(f'text/text_page{pno + 1}.txt', 'w') as file:
            file.write(func(text))


if __name__ == '__main__':
    doc = fitz.open('05_Book_of_the_Dead.pdf')
    # pno = 177
    # page = doc.load_page(pno)
    # text = page.get_text()
    # print(text)
    # print(repr(text))
    extract_images_from_doc(doc)

    for page_index in range(len(doc)):
        # get the page itself
        page = doc.load_page(page_index)
        extract_text(page, page_index)
        # extract_images(page, page_index)
