import asyncio
import datetime as dt
import json
import logging.config
import os
from enum import StrEnum
from typing import Tuple

import aiohttp
import pytz
from deepdiff import DeepDiff
from furl import furl

ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))

logging.config.fileConfig(os.path.join(ROOT_DIR, 'logging.conf'), disable_existing_loggers=False)
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


class StorageManager:
    def __init__(self, root_folder: str) -> None:
        self._root_folder = root_folder

    def get_root_folder(self) -> str:
        return self._root_folder

    def create_root_folder(self) -> None:
        if not os.path.exists(self._root_folder):
            os.mkdir(self._root_folder)
            logger.info(f"ROOT Folder {self._root_folder} created!")
        else:
            logger.warning(f"ROOT Folder {self._root_folder} already exists")

    def create_folder(self, folder: str) -> None:
        folder_path = f'{self._root_folder}/{folder}'
        if not os.path.exists(folder_path):
            os.mkdir(folder_path)
            logger.info(f"Folder {folder_path} created!")
        else:
            logger.warning(f"Folder {folder_path} already exists")

    def save_json(self, file_name: str, the_json: dict, folder: str = None) -> None:
        if folder:
            file_path = f'{self._root_folder}/{folder}/{file_name}.json'
        else:
            file_path = f'{self._root_folder}/{file_name}.json'
        with open(file_path, 'w') as file:
            json.dump(the_json, file, indent=2)

    def save_txt(self, file_name: str, txt: str, folder: str = None) -> None:
        if folder:
            file_path = f'{self._root_folder}/{folder}/{file_name}.json'
        else:
            file_path = f'{self._root_folder}/{file_name}.json'
        with open(file_path, 'w') as file:
            file.write(txt)


async def provide(the_url: furl) -> Tuple[int, dict]:
    async with aiohttp.ClientSession() as session:
        async with session.get(the_url.url) as response:
            if response.ok:
                return response.status, await response.json()
            logger.info(f"Response not OK -> {response}")
            return response.status, {}


class DiffType(StrEnum):
    SOURCE = 'SOURCE'
    TARGET = 'TARGET'


class DCSConfigRetrieval:
    def __init__(self, storage_manager: StorageManager):
        self._storage_manager = storage_manager

    def retrieve(self, namespace: str, state: str, cluster_domain: str, diff_type: DiffType) -> dict:
        json_dir = f'{diff_type}-{namespace}-{cluster_domain}'
        self._storage_manager.create_folder(json_dir)
        logger.info(f">>>> Retrieving DCS config for {namespace.upper()} {cluster_domain.upper()}")
        env_base_url = furl(
            f'http://dynamic-config-service.{namespace}.{cluster_domain}.aws-us-east-1.{cluster_domain}.williamhill.plc/')
        logger.info(f">> Environment URL: {env_base_url}")
        services_url = env_base_url
        services_url.path = 'cop/services'
        _, services = asyncio.run(provide(env_base_url))
        pretty_json = json.dumps(services, indent=2)
        logger.info(f">> Services: {pretty_json}")

        env_config = dict()
        for service in services:
            service_url = env_base_url
            service_url.path = f'config/{state}/{service}'
            status, the_json = asyncio.run(provide(service_url))
            env_config[service] = the_json
            pretty_json = json.dumps(the_json, indent=2)
            logger.info(f">> Got DCS config for Service '{service}'")
            logger.debug(pretty_json)

            self._storage_manager.save_json(service, the_json, json_dir)

        return env_config


def environment_title(env: dict) -> str:
    return f"{env['namespace'].upper()}-{env['cluster'].upper()}"


def get_new_data_time() -> str:
    return str(dt.datetime.now(tz=pytz.utc).strftime("%d-%m-%YT%H:%M:%S"))


def main(root_dir: str):
    now = get_new_data_time()
    main_folder_name = f'{root_dir}/diff_run_{now}'
    storage_manager = StorageManager(main_folder_name)
    storage_manager.create_root_folder()

    source = {'namespace': 'nv-rtl', 'state': 'nv', 'cluster': 'usp'}
    target = {'namespace': 'nv-wh-rtl', 'state': 'nv-wh', 'cluster': 'usc'}

    dcs_config_retrieval = DCSConfigRetrieval(storage_manager)

    source_dcs_config = dcs_config_retrieval.retrieve(source['namespace'], source['state'], source['cluster'], DiffType.SOURCE)
    logger.debug(f"\n>>> {source_dcs_config}")

    target_dcs_config = dcs_config_retrieval.retrieve(target['namespace'], target['state'], target['cluster'], DiffType.TARGET)
    logger.debug(f"\n >>> {target_dcs_config}")

    logger.info(f"\n\n{environment_title(source)} vs. {environment_title(target)} DCS config:")
    env_diff = DeepDiff(
        source_dcs_config,
        target_dcs_config,
        verbose_level=2
    )
    parsed_diff_json = json.loads(env_diff.to_json())
    logger.info(json.dumps(parsed_diff_json, indent=2))

    json_diff_file_name = f"diff_{environment_title(source)}_vs_{environment_title(target)}"
    storage_manager.save_json(json_diff_file_name, parsed_diff_json)
    summary_json = {
        "time": now,
        "source": {
            "name": environment_title(source),
            "full_dcs_config": source_dcs_config
        },
        "target": {
            "name": environment_title(target),
            "full_dcs_config": target_dcs_config
        },
        "diff": parsed_diff_json

    }

    json_summary_file_name = f"./full_summary_{environment_title(source)}_vs_{environment_title(target)}"
    storage_manager.save_json(json_summary_file_name, summary_json)


ROOT_DIR = os.path.realpath(os.path.dirname(__file__), )

if __name__ == '__main__':
    main(ROOT_DIR)
